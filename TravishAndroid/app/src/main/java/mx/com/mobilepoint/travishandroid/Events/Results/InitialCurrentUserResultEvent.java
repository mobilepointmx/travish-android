package mx.com.mobilepoint.travishandroid.Events.Results;

import mx.com.mobilepoint.travishandroid.Events.InitialCurrentUserEvent;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.UserDetails;

/**
 * Created by IvanIsrael on 09/11/2015.
 */
public class InitialCurrentUserResultEvent {
    public UserDetails user;

    public InitialCurrentUserResultEvent(UserDetails user){
        this.user = user;
    }
}
