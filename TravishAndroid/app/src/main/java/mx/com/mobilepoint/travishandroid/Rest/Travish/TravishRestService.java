package mx.com.mobilepoint.travishandroid.Rest.Travish;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.IOException;

import de.greenrobot.event.EventBus;
import mx.com.mobilepoint.travishandroid.Constants.TravishConstants;
import mx.com.mobilepoint.travishandroid.Events.ApiErrorCreditCardEvent;
import mx.com.mobilepoint.travishandroid.Events.ApiErrorEvent;
import mx.com.mobilepoint.travishandroid.Events.BrainTreeTokenEvent;
import mx.com.mobilepoint.travishandroid.Events.CreditCardEvent;
import mx.com.mobilepoint.travishandroid.Events.InitialCurrentUserEvent;
import mx.com.mobilepoint.travishandroid.Events.LoginEvent;
import mx.com.mobilepoint.travishandroid.Events.RegisterEvent;
import mx.com.mobilepoint.travishandroid.Events.Results.BrainTreeTokenResultEvent;
import mx.com.mobilepoint.travishandroid.Events.Results.CreditCardResultEvent;
import mx.com.mobilepoint.travishandroid.Events.Results.InitialCurrentUserResultEvent;
import mx.com.mobilepoint.travishandroid.Events.Results.LoginResultEvent;
import mx.com.mobilepoint.travishandroid.Events.Results.RegisterResultEvent;
import mx.com.mobilepoint.travishandroid.Events.Results.UberResultEvent;
import mx.com.mobilepoint.travishandroid.Events.Results.UbetTokenResultEvent;
import mx.com.mobilepoint.travishandroid.Events.UberEvent;
import mx.com.mobilepoint.travishandroid.Events.UberTokenEvent;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.BrainTreeToken;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.ClientBrainTreeResponse;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.TravishCredentials;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.UberTokenResponse;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.User;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.UserDetails;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.UserResponse;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.UserUberCredentials;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by IvanIsrael on 05/11/2015.
 */
public class TravishRestService {
    private static final String TAG = TravishRestService.class.getName();

    private TravishApi api;
    private EventBus bus;

    public TravishRestService(TravishApi api, EventBus bus){
        this.bus = bus;
        this.api = api;
    }

    public void setApi(TravishApi api) {
        this.api = api;
    }


    public void onEventMainThread(RegisterEvent event){
        Log.e(TAG, "Register event");
        Gson gson = new Gson();
        String json = gson.toJson(event.registerDetails);
        Log.e("json tags ", json);
        Call<UserResponse> resp = api.signUpUser(event.registerDetails);
        resp.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Response<UserResponse> response, Retrofit retrofit) {
                Log.e("status code: ", String.valueOf(response.code()));
                if(response.isSuccess()){
                    Log.e("response body: ", response.body().getUser().getUsername());
                    bus.post(new RegisterResultEvent(response.body().getUser(), "Se ha registrado con éxito"));
                }else{
                    String error = "";
                    try {
                        error = response.raw().body().toString();
                        Log.e("json response ", error);
                        Gson gson = new Gson();
                        String json = gson.toJson(response.raw().body());
                        Log.e("json response ", json);
                        //error = response.errorBody().string().toString();
                        JsonElement ele = new JsonParser().parse(response.errorBody().string());
                        Log.e("mensaje body: ", ele.getAsJsonObject().get("errors").getAsJsonArray().get(0).getAsJsonObject().get("message").toString());
                        bus.post(new ApiErrorEvent(ele.getAsJsonObject().get("errors").getAsJsonArray().get(0).getAsJsonObject().get("message").toString()));
                    } catch (IOException e) {
                        bus.post(new ApiErrorEvent(e.getMessage()));
                        Log.e("catch mensaje: ", e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("Fallo rest ", t.getMessage());
                bus.post(new ApiErrorEvent(t.getMessage()));

            }
        });

    }


    /*
    *Login method
     */
    public void onEventMainThread(LoginEvent event){
        Log.e(TAG, "Login event");
        Gson gson = new Gson();
        String clientId = "MobileAndroid";
        String clientSecret = "TravishAndoid";
        String grantType = "password";

        Call<TravishCredentials> resp = api.loginUser(event.user.getUsername(), event.user.getPassword(), clientId, clientSecret, grantType);
        resp.enqueue(new Callback<TravishCredentials>() {
            @Override
            public void onResponse(Response<TravishCredentials> response, Retrofit retrofit) {
                Log.e("status code: ", String.valueOf(response.code()));
                if(response.isSuccess()){

                    bus.post(new LoginResultEvent(response.body()));
                }else{
                    String error = "";
                    try {
                        if(response.code()==403){
                            JsonElement ele = new JsonParser().parse(response.errorBody().string());
                            bus.post(new ApiErrorEvent(ele.getAsJsonObject().get("error").getAsString()));
                        }else{
                            error = response.raw().body().toString();
                            Log.e("json response ", error);
                            Gson gson = new Gson();
                            String json = gson.toJson(response.raw().body());
                            Log.e("json response ", json);
                            //error = response.errorBody().string().toString();
                            JsonElement ele = new JsonParser().parse(response.errorBody().string());
                            Log.e("mensaje body: ", ele.getAsJsonObject().get("errors").getAsJsonArray().get(0).getAsJsonObject().get("message").toString());
                            bus.post(new ApiErrorEvent(ele.getAsJsonObject().get("errors").getAsJsonArray().get(0).getAsJsonObject().get("message").toString()));
                        }

                    } catch (IOException e) {
                        bus.post(new ApiErrorEvent(e.getMessage()));
                        Log.e("catch mensaje: ", e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("Fallo rest ", t.getMessage());
                bus.post(new ApiErrorEvent(t.getMessage()));

            }
        });

    }


    public void onEventMainThread(BrainTreeTokenEvent event){
        Log.e(TAG, "token event");

        Call<BrainTreeToken> resp = api.getBrainTreeToken();
                resp.enqueue(new Callback<BrainTreeToken>() {
            @Override
            public void onResponse(Response<BrainTreeToken> response, Retrofit retrofit) {
                Log.e("status code: ", String.valueOf(response.code()));
                if(response.isSuccess()){
                    Log.e("body: ", String.valueOf(response.body().getToken()));
                    bus.post(new BrainTreeTokenResultEvent(response.body()));
                }else{
                    String error = "";
                    try {
                        if(response.code()==403){
                            JsonElement ele = new JsonParser().parse(response.errorBody().string());
                            bus.post(new ApiErrorEvent(ele.getAsJsonObject().get("error").getAsString()));
                        }else{
                            error = response.raw().body().toString();
                            Log.e("json response ", error);
                            Gson gson = new Gson();
                            String json = gson.toJson(response.raw().body());
                            Log.e("json response ", json);
                            //error = response.errorBody().string().toString();
                            JsonElement ele = new JsonParser().parse(response.errorBody().string());
                            Log.e("mensaje body: ", ele.getAsJsonObject().get("errors").getAsJsonArray().get(0).getAsJsonObject().get("message").toString());
                            bus.post(new ApiErrorEvent(ele.getAsJsonObject().get("errors").getAsJsonArray().get(0).getAsJsonObject().get("message").toString()));
                        }

                    } catch (IOException e) {
                        bus.post(new ApiErrorEvent(e.getMessage()));
                        Log.e("catch mensaje: ", e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("Fallo rest ", t.getMessage());
                bus.post(new ApiErrorEvent(t.getMessage()));

            }
        });

    }

    public void onEventMainThread(CreditCardEvent event){
        Log.e(TAG, "token event");

        Call<ClientBrainTreeResponse> resp = api.postCreditCard(event.creditCardRequest);
        resp.enqueue(new Callback<ClientBrainTreeResponse>() {
            @Override
            public void onResponse(Response<ClientBrainTreeResponse> response, Retrofit retrofit) {
                Log.e("status code: ", String.valueOf(response.code()));
                if(response.isSuccess()){
                    Log.e("body: ", String.valueOf(response.body()));
                    bus.post(new CreditCardResultEvent(response.body()));
                }else{
                    String error = "";
                    try {
                        if(response.code()==403){
                            JsonElement ele = new JsonParser().parse(response.errorBody().string());
                            bus.post(new ApiErrorEvent(ele.getAsJsonObject().get("error").getAsString()));
                        }else{
                            error = response.raw().body().toString();
                            Log.e("json response ", error);
                            Gson gson = new Gson();
                            String json = gson.toJson(response.raw().body());
                            Log.e("json response ", json);
                            bus.post(new ApiErrorCreditCardEvent("Error api"));
                            //error = response.errorBody().string().toString();
                            //JsonElement ele = new JsonParser().parse(response.errorBody().string());
                            //Log.e("mensaje body: ", ele.getAsJsonObject().get("errors").getAsJsonArray().get(0).getAsJsonObject().get("message").toString());
                            //bus.post(new ApiErrorEvent(ele.getAsJsonObject().get("errors").getAsJsonArray().get(0).getAsJsonObject().get("message").toString()));
                        }

                    } catch (IOException e) {
                        bus.post(new ApiErrorCreditCardEvent(e.getMessage()));
                        Log.e("catch mensaje: ", e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                bus.post(new ApiErrorCreditCardEvent(t.getMessage()));

            }
        });

    }

    /*
    * Get User
    * */
    public void onEventMainThread(InitialCurrentUserEvent event){

        Call<UserDetails> resp = api.getUser();
        resp.enqueue(new Callback<UserDetails>() {
            @Override
            public void onResponse(Response<UserDetails> response, Retrofit retrofit) {
                Log.e("status code: ", String.valueOf(response.code()));
                if(response.isSuccess()){
                    Gson gson = new Gson();
                    String json = gson.toJson(response.body());
                    bus.post(new InitialCurrentUserResultEvent(response.body()));
                }else{
                    String error = "";
                    try {
                        if(response.code()==403){
                            JsonElement ele = new JsonParser().parse(response.errorBody().string());
                            bus.post(new ApiErrorEvent(ele.getAsJsonObject().get("error").getAsString()));
                        }else{
                            bus.post(new ApiErrorEvent("Error"));
                        }

                    } catch (IOException e) {
                        bus.post(new ApiErrorEvent(e.getMessage()));
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                bus.post(new ApiErrorEvent(t.getMessage()));

            }
        });

    }

    public void onEventMainThread(UberTokenEvent event){
        UberTokenResponse prueba = new UberTokenResponse();
        Gson gson = new Gson();
        String json = gson.toJson(prueba);
        Log.e("json uber ", json);
        Call<UberTokenResponse> resp = api.postUberToken(event.uber);
        resp.enqueue(new Callback<UberTokenResponse>() {
            @Override
            public void onResponse(Response<UberTokenResponse> response, Retrofit retrofit) {
                Log.e("status code: ", String.valueOf(response.code()));
                if(response.isSuccess()){
                    Gson gson = new Gson();
                    String json = gson.toJson(response.body());
                    bus.post(new UbetTokenResultEvent(response.body()));
                }else{
                    String error = "";
                    Gson gson = new Gson();
                    String json = gson.toJson(response.body());
                    Log.e("json uber resp ", json);
                    try {
                        if(response.code()==403){
                            JsonElement ele = new JsonParser().parse(response.errorBody().string());
                            bus.post(new ApiErrorEvent(ele.getAsJsonObject().get("error").getAsString()));
                        }else{
                            bus.post(new ApiErrorEvent("Error"));
                        }

                    } catch (IOException e) {
                        bus.post(new ApiErrorEvent(e.getMessage()));
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                bus.post(new ApiErrorEvent(t.getMessage()));

            }
        });

    }
}
