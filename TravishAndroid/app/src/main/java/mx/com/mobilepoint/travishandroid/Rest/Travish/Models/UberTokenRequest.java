package mx.com.mobilepoint.travishandroid.Rest.Travish.Models;

/**
 * Created by IvanIsrael on 12/11/2015.
 */
public class UberTokenRequest {
    private UserUberCredentials uber_access;


    public UserUberCredentials getUber_access() {
        return uber_access;
    }

    public void setUber_access(UserUberCredentials uber_access) {
        this.uber_access = uber_access;
    }
}
