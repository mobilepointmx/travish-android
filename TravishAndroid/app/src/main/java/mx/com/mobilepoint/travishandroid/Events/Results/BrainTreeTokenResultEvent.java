package mx.com.mobilepoint.travishandroid.Events.Results;

import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.BrainTreeToken;

/**
 * Created by IvanIsrael on 09/11/2015.
 */
public final class BrainTreeTokenResultEvent {
    public BrainTreeToken token;

    public BrainTreeTokenResultEvent(BrainTreeToken token){
        this.token = token;
    }
}
