package mx.com.mobilepoint.travishandroid.Rest.Travish.Models;

/**
 * Created by IvanIsrael on 09/11/2015.
 */
public class BrainTreeToken {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
