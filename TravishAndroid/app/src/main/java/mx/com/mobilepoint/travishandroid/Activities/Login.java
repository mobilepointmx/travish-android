package mx.com.mobilepoint.travishandroid.Activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.greenrobot.event.EventBus;
import mx.com.mobilepoint.travishandroid.Events.ApiErrorEvent;
import mx.com.mobilepoint.travishandroid.Events.LoginEvent;
import mx.com.mobilepoint.travishandroid.Events.Results.LoginResultEvent;
import mx.com.mobilepoint.travishandroid.R;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.User;
import mx.com.mobilepoint.travishandroid.widgets.EditTextBackEvent;

/**
 * Created by luisalejandro on 30/10/2015.
 */
public class Login extends AppCompatActivity {
    public EditText et_username;
    public EditText et_password;
    private EventBus bus = EventBus.getDefault();
    private CircularProgressView progressView;
    ProgressDialog progress;
    Button btnSubmit;
    private View containerView;
    private static final String TAG = CreateUser.class.getName();
    User user;
    private ImageView logo;
    private int logoHeight;
    private int windowHeight;
    String username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        windowHeight = size.y;
        bus.register(this);
        progressView = (CircularProgressView) findViewById(R.id.pb_loading);
        containerView = findViewById(R.id.container);
        containerView.requestFocus();

        et_username = (EditText) findViewById(R.id.ET_Email);
        et_password = (EditText) findViewById(R.id.ET_Pass);

        logo = (ImageView)findViewById(R.id.ImgLogin);

        logoHeight = logo.getLayoutParams().height;

        et_password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                loginUser(null);
                return true;
            }
        });

        LoginFocusListener focusListener = new LoginFocusListener();
        et_username.setOnFocusChangeListener(focusListener);
        et_password.setOnFocusChangeListener(focusListener);

        BackListener backListener = new BackListener();

        //et_username.setOnEditTextImeBackListener(backListener);
        //et_password.setOnEditTextImeBackListener(backListener);

        et_username.setOnClickListener(focusListener);
        et_password.setOnClickListener(focusListener);

    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        bus.unregister(this);
    }

    public void registerUser(View view){
        Intent ventana = new Intent(Login.this, CreateUser.class);
        startActivity(ventana);
    }

    public void loginUser(View view){
        username = et_username.getText().toString().trim();
        password = et_password.getText().toString().trim();

        if(isEmpty() || !isEmail()) return;

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        showProgress(true);

        progressView.startAnimation();
       //progress = ProgressDialog.show(Login.this, "Travish", "Loggin..", true);
        user = new User(username, password);
        bus.post(new LoginEvent(user));

    }

    public void onEventMainThread(ApiErrorEvent event){
        showProgress(false);
       // progress.dismiss();
        if(event.message.equals("invalid_grant")){
            Toast.makeText(this, "Invalid Username or Password.", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this, event.message, Toast.LENGTH_LONG).show();
        }
    }

    public boolean isEmpty(){
        if(username.length() == 0 || username == null){
            Toast.makeText(this, "Username is empty.", Toast.LENGTH_LONG).show();
            return true;
        }
        if(password.length() == 0 || username == null){
            Toast.makeText(this, "Password is empty.", Toast.LENGTH_LONG).show();
            return true;
        }

        return false;
    }

    public boolean isEmail(){
        String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(PATTERN_EMAIL);

        // Match the given input against this pattern
        Matcher matcher = pattern.matcher(username);
        if(matcher.matches()) return true;

        Toast.makeText(this, "Username must be an email.", Toast.LENGTH_LONG).show();
        return false;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    protected void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            containerView.setVisibility(show ? View.GONE : View.VISIBLE);
            containerView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    containerView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.

            containerView.setVisibility(show ? View.GONE : View.VISIBLE);
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }


    class LoginFocusListener implements View.OnFocusChangeListener, View.OnClickListener{

        @Override
        public void onFocusChange(View view, boolean focusGained) {

            if(windowHeight > 900){
                return;
            }

            if(focusGained){

                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(logo.getLayoutParams().width, logoHeight / 2);
                logo.setLayoutParams(layoutParams);

            }else{
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(logo.getLayoutParams().width, logoHeight);
                logo.setLayoutParams(layoutParams);
            }
        }

        @Override
        public void onClick(View view) {
            if(windowHeight > 900){
                return;
            }
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(logo.getLayoutParams().width, logoHeight / 2);
            logo.setLayoutParams(layoutParams);
        }
    }

    class BackListener implements EditTextBackEvent.EditTextImeBackListener{
        @Override
        public void onImeBack(EditTextBackEvent ctrl, String text) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(logo.getLayoutParams().width, logoHeight);
            logo.setLayoutParams(layoutParams);
        }
    }
}
