package mx.com.mobilepoint.travishandroid.Events.Results;

import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.TravishCredentials;

/**
 * Created by IvanIsrael on 06/11/2015.
 */
public final class LoginResultEvent {
    public TravishCredentials credentials;

    public LoginResultEvent(TravishCredentials credentials){
        this.credentials = credentials;
    }
}
