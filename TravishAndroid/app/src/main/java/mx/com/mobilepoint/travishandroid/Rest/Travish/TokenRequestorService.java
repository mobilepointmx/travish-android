package mx.com.mobilepoint.travishandroid.Rest.Travish;

import android.util.Log;

import java.io.IOException;

import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.TravishCredentials;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by IvanIsrael on 05/11/2015.
 */
public class TokenRequestorService implements AccessTokenRequestor {

    private static final String TAG = TokenRequestorService.class.getName();
    private TravishTokenApi api;

    public static TokenRequestorService instance(TravishTokenApi api) {
        TokenRequestorService service = new TokenRequestorService();
        service.api = api;
        return service;
    }

    @Override
    public TravishCredentials refreshToken(String token) throws IOException {
        String clientId = "tattlerMobileApp";
        String clientSecret = "tattler@mobile";
        String grantType = "refresh_token";

        Call<TravishCredentials>  accessResponse = null;


        accessResponse = api.refreshToken(token, clientId, clientSecret, grantType);
        TravishCredentials credentials = accessResponse.execute().body();

        return credentials;
    }
}
