package mx.com.mobilepoint.travishandroid.Events.Results;

import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.UserUberCredentials;

/**
 * Created by IvanIsrael on 12/11/2015.
 */
public final class UberResultEvent {
    public UserUberCredentials uber;
    public UberResultEvent(UserUberCredentials body) {
        this.uber = body;
    }
}
