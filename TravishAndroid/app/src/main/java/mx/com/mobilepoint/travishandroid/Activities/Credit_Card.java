package mx.com.mobilepoint.travishandroid.Activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.Card;
import com.braintreepayments.api.exceptions.BraintreeError;
import com.braintreepayments.api.exceptions.ErrorWithResponse;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.braintreepayments.api.interfaces.BraintreeErrorListener;
import com.braintreepayments.api.interfaces.PaymentMethodNonceCreatedListener;
import com.braintreepayments.api.models.CardBuilder;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.github.rahatarmanahmed.cpv.CircularProgressView;

import java.util.Calendar;

import de.greenrobot.event.EventBus;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;
import mx.com.mobilepoint.travishandroid.Events.ApiErrorCreditCardEvent;
import mx.com.mobilepoint.travishandroid.Events.BrainTreeTokenEvent;
import mx.com.mobilepoint.travishandroid.Events.CreditCardEvent;
import mx.com.mobilepoint.travishandroid.Events.Results.BrainTreeTokenResultEvent;
import mx.com.mobilepoint.travishandroid.Events.Results.CreditCardResultEvent;
import mx.com.mobilepoint.travishandroid.R;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.BillingAddress;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.ClientBrainTree;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.CreditCardRequest;
import mx.com.mobilepoint.travishandroid.TravishApplication;
import mx.com.mobilepoint.travishandroid.Utils.ApplicationStateHelper;


/**
 * Created by luisalejandro on 03/11/2015.
 */
public class Credit_Card extends AppCompatActivity implements View.OnClickListener, PaymentMethodNonceCreatedListener,BraintreeErrorListener {

    private static final String KEY_CLIENT_TOKEN = "clientToken";
    private static final String KEY_NONCE = "nonce";
    private static final String KEY_ENVIRONMENT = "environment";
    private static final String TAG = Credit_Card.class.getName();

    private EventBus bus = EventBus.getDefault();
    private CircularProgressView progressView;
    private View containerView2;
    BraintreeFragment mBraintreeFragment;
    CardBuilder cardBuilder;

    private String mClientToken = null;
    private String mNonce = null;
    private int mEnvironment;
    private int MY_SCAN_REQUEST_CODE = 0;
    String cvv;
    String creditCard;
    String expirationDate;
    String address1;
    String zipCode;
    String phone;
    String cardHolderName;
    String city;
    String addres2;
    String nonce;
    private CreditCardRequest creditCardRequest;
    Context context = this;

    private Button btn_submit;
    private EditText et_creditCard;
    private EditText et_expirationDate;
    private EditText et_cvv;
    private EditText et_holderName;
    private EditText et_address1;
    private EditText et_address2;
    private EditText et_city;
    private EditText et_zipCode;
    private EditText et_phone;
    private Spinner sp_country;
    private Spinner sp_state;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_creditcard_);
        btn_submit = (Button)findViewById(R.id.btn_Submit);
        et_address1 = (EditText)findViewById(R.id.ET_Addres1);
        et_address2 = (EditText)findViewById(R.id.ET_Addres2);
        et_city = (EditText)findViewById(R.id.ET_City);
        et_creditCard = (EditText)findViewById(R.id.ET_CreditCard);
        et_cvv = (EditText)findViewById(R.id.ET_CVV);
        et_expirationDate = (EditText)findViewById(R.id.ET_YearCC);
        et_zipCode = (EditText)findViewById(R.id.ET_Zip);
        et_phone = (EditText)findViewById(R.id.ET_Phone);
        et_holderName = (EditText)findViewById(R.id.ET_CardHolder);
        sp_country = (Spinner)findViewById(R.id.sp_country);
        sp_state = (Spinner)findViewById(R.id.sp_state);
        containerView2 = findViewById(R.id.container);
        progressView = (CircularProgressView)findViewById(R.id.pb_loading);
        //Watch format for date input
        TextWatcher tw = new TextWatcher() {
            private String current = "";
            private String mmyyyy = "      ";
            private Calendar cal = Calendar.getInstance();

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals(current)) {
                    String clean = s.toString().replaceAll("[^\\d.]", "");
                    String cleanC = current.replaceAll("[^\\d.]", "");

                    int cl = clean.length();
                    int sel = cl;
                    for (int i = 2; i <= cl && i < 6; i += 2) {
                        sel++;
                    }
                    //Fix for pressing delete next to a forward slash
                    if (clean.equals(cleanC)) sel--;

                    if (clean.length() < 6){
                        clean = clean + mmyyyy.substring(clean.length());
                    }else{
                        //This part makes sure that when we finish entering numbers
                        //the date is correct, fixing it otherwise
                        //int day  = Integer.parseInt(clean.substring(0,2));
                        int mon  = Integer.parseInt(clean.substring(0,2));
                        int year = Integer.parseInt(clean.substring(2,6));

                        if(mon > 12) mon = 12;
                        cal.set(Calendar.MONTH, mon-1);
                        year = (year<1900)?1900:(year>2100)?2100:year;
                        cal.set(Calendar.YEAR, year);
                        // ^ first set year for the line below to work correctly
                        //with leap years - otherwise, date e.g. 29/02/2012
                        //would be automatically corrected to 28/02/2012

                        //day = (day > cal.getActualMaximum(Calendar.DATE))? cal.getActualMaximum(Calendar.DATE):day;
                        clean = String.format("%02d%02d", mon, year);
                    }

                    clean = String.format("%s/%s",
                            clean.substring(0, 2),
                            clean.substring(2, 6));

                    sel = sel < 0 ? 0 : sel;
                    current = clean;
                    et_expirationDate.setText(current);
                    et_expirationDate.setSelection(sel < current.length() ? sel : current.length());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {}
        };

        et_expirationDate.addTextChangedListener(tw);
        btn_submit.setEnabled(false);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(KEY_CLIENT_TOKEN)) {
                mClientToken = savedInstanceState.getString(KEY_CLIENT_TOKEN);
                setup();
            }
            if (savedInstanceState.containsKey(KEY_NONCE)) {
                mNonce = savedInstanceState.getString(KEY_NONCE);
            }
        }
        setup();
        btn_submit.setOnClickListener(this);
    }
    @Override
    public void onBackPressed() {
        Log.e("BACKP", "OnBACKPre");
        bus.unregister(this);
        Log.e("ondestroy", "onDestroy");
        ((TravishApplication) getApplicationContext()).clearSession();
        finish();
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.e("ondestroy", "onDestroy");
    }

    public void getClientToken(){
        try {
            String authorization;
            if (mClientToken != null) {
                authorization = mClientToken;
                mBraintreeFragment = BraintreeFragment.newInstance(this, authorization);
                enableButtons(true);
            }else{
                bus.post(new BrainTreeTokenEvent());
            }

        } catch (InvalidArgumentException e) {
            Log.e(TAG, e.getMessage());
        }

    }

    public void onEventMainThread(BrainTreeTokenResultEvent event){
        try {
            String authorization;
            authorization = event.token.getToken();
            if(mBraintreeFragment == null)
                Log.e(TAG, "igual a null!!!");
            mBraintreeFragment = BraintreeFragment.newInstance(this, authorization);
            if(mBraintreeFragment != null)
                Log.e(TAG, "distinto de null!!!");
            enableButtons(true);
            Log.e(TAG, event.token.getToken());
        } catch (InvalidArgumentException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public void onEventMainThread(ApiErrorCreditCardEvent event){
        showProgress(false);
        Toast.makeText(this, event.message, Toast.LENGTH_LONG).show();

    }

    @Override
    public void onClick(View view) {
        showProgress(true);
        if(verifyEmptyInput() && verifyCardNumber() && verifyCVV())
            verifyCard();
        else
            showProgress(false);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mClientToken != null) {
            outState.putString(KEY_CLIENT_TOKEN, mClientToken);
        }
        if (mNonce != null) {
            outState.putString(KEY_NONCE, mNonce);
        }
    }

    private void verifyCard(){
        try{
            Log.e(TAG, et_expirationDate.getText().toString().trim());
            cardBuilder = new CardBuilder();
            cardBuilder.cardNumber(et_creditCard.getText().toString().trim());
            cardBuilder.cvv(et_cvv.getText().toString().trim());
            cardBuilder.expirationDate(et_expirationDate.getText().toString().trim());

            Card.tokenize(mBraintreeFragment, cardBuilder);
        }catch (Exception e){
            showProgress(false);
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public boolean verifyCardNumber(){
        if(et_creditCard.getText().toString().trim().length() == 16) return true;
        Toast.makeText(this, "Invalid Credit Card", Toast.LENGTH_LONG ).show();
        return false;
    }

    public boolean verifyCVV(){
        String cvv = et_cvv.getText().toString().trim();
        if(cvv.length() > 2 && cvv.length() <5) return true;
        Toast.makeText(this, "Invalid CVV", Toast.LENGTH_LONG ).show();
        return false;
    }

    public boolean verifyEmptyInput(){
        cvv = et_cvv.getText().toString().trim();
        creditCard = et_creditCard.getText().toString().trim();
        expirationDate = et_expirationDate.getText().toString().trim();
        address1 = et_address1.getText().toString().trim();
        zipCode = et_zipCode.getText().toString().trim();
        phone = et_phone.getText().toString().trim();
        cardHolderName = et_holderName.getText().toString().trim();
        city = et_city.getText().toString().trim();

        if(creditCard.isEmpty()){
            Toast.makeText(this, "Credit Card Number is required", Toast.LENGTH_LONG ).show();
            return false;
        }
        if(cvv.isEmpty()){
            Toast.makeText(this, "CVV is required", Toast.LENGTH_LONG ).show();
            return false;
        }
        if(expirationDate.isEmpty()){
            Toast.makeText(this, "Expiration Date is required", Toast.LENGTH_LONG ).show();
            return false;
        }
        if(address1.isEmpty()){
            Toast.makeText(this, "An Address is required", Toast.LENGTH_LONG ).show();
            return false;
        }
        if(zipCode.isEmpty()){
            Toast.makeText(this, "Zipcode is required", Toast.LENGTH_LONG ).show();
            return false;
        }
        if(cardHolderName.isEmpty()){
            Toast.makeText(this, "Card Holder Name is required", Toast.LENGTH_LONG ).show();
            return false;
        }
        if(phone.isEmpty()){
            Toast.makeText(this, "A number phone is required", Toast.LENGTH_LONG ).show();
            return false;
        }
        if(city.isEmpty()){
            Toast.makeText(this, "City is required", Toast.LENGTH_LONG ).show();
            return false;
        }

        return true;
    }

    @Override
    public void onPaymentMethodNonceCreated(PaymentMethodNonce paymentMethod) {
        // Send this nonce to your server
        String nonce = paymentMethod.getNonce();
        Log.e(TAG, nonce);
        sendData(nonce);
    }

    public void onEventMainThread(CreditCardResultEvent event){
        ApplicationStateHelper.doRestart(getApplication());

//        Intent intent = new Intent(this, Login.class);
//        //Intent intent = new Intent(this, ProfileWizardActivity.class);
//        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);
//        finish();
        //Toast.makeText(this, event.client.getCustomerId(), Toast.LENGTH_LONG ).show();
    }

    public void sendData(String nonce){

        progressView.startAnimation();
        String addres2 =et_address2.getText().toString().trim();
        BillingAddress b_address = new BillingAddress();
        creditCardRequest = new CreditCardRequest();
        b_address.setCountryName("Mexico");
        b_address.setRegion("Yucatan");
        b_address.setLocality(city);
        b_address.setStreetAddress(address1 + " " + addres2);
        b_address.setPostalCode(zipCode);
        ClientBrainTree clientBrainTree = new ClientBrainTree();

        mx.com.mobilepoint.travishandroid.Rest.Travish.Models.CreditCard creditCardT;
        creditCardT = new mx.com.mobilepoint.travishandroid.Rest.Travish.Models.CreditCard();

        creditCardT.setBillingAddress(b_address);
        creditCardT.setCardholderName(cardHolderName);
        clientBrainTree.setPaymentMethodNonce(nonce);
        clientBrainTree.setCreditCard(creditCardT);
        creditCardRequest.setClient(clientBrainTree);

        bus.post(new CreditCardEvent(creditCardRequest));
    }

    @Override
    public void onError(Exception error) {
        showProgress(false);
        if (error instanceof ErrorWithResponse) {
            ErrorWithResponse errorWithResponse = (ErrorWithResponse) error;
            BraintreeError cardErrors = ((ErrorWithResponse) error).errorFor("creditCard");
            //Toast.makeText(this, errorWithResponse.getErrorResponse().toString(), Toast.LENGTH_LONG ).show();
            Log.e(TAG, errorWithResponse.getErrorResponse().toString());

            if (cardErrors != null) {
                // There is an issue with the credit card.
                BraintreeError expirationMonthError = cardErrors.errorFor("expirationMonth");
                BraintreeError creditCardError = cardErrors.errorFor("creditCard");
                BraintreeError cardNumberError = cardErrors.errorFor("cardNumber");
                BraintreeError ccvError = cardErrors.errorFor("cvv");
                BraintreeError numberError = cardErrors.errorFor("number");
                BraintreeError expirationDateError = cardErrors.errorFor("expirationDate");
                BraintreeError expirationYearError = cardErrors.errorFor("expirationYear");

                if (numberError != null) {
                    // There is an issue with the expiration month.
                    Toast.makeText(this, numberError.getMessage(), Toast.LENGTH_LONG ).show();
                    // setErrorMessage(expirationMonthError.getMessage());
                    return;
                }

                if (ccvError != null) {
                    // There is an issue with the expiration month.
                    Toast.makeText(this, ccvError.getMessage(), Toast.LENGTH_LONG ).show();
                    // setErrorMessage(expirationMonthError.getMessage());
                    return;
                }
                if (expirationYearError != null) {
                    // There is an issue with the expiration month.
                    Toast.makeText(this, expirationYearError.getMessage(), Toast.LENGTH_LONG ).show();
                    // setErrorMessage(expirationMonthError.getMessage());
                    return;
                }
                if (expirationDateError != null) {
                    // There is an issue with the expiration month.
                    Toast.makeText(this, expirationDateError.getMessage(), Toast.LENGTH_LONG ).show();
                    // setErrorMessage(expirationMonthError.getMessage());
                    return;
                }
                if (creditCardError != null) {
                    // There is an issue with the expiration month.
                    Toast.makeText(this, creditCardError.getMessage(), Toast.LENGTH_LONG ).show();
                    // setErrorMessage(expirationMonthError.getMessage());
                    return;
                }
                //Log.e(TAG, errorWithResponse.getCause().getMessage().toString());
                if (cardNumberError != null) {
                    // There is an issue with the expiration month.
                    Toast.makeText(this, cardNumberError.getMessage(), Toast.LENGTH_LONG ).show();
                    // setErrorMessage(expirationMonthError.getMessage());
                    return;
                }
                if (expirationMonthError != null) {
                    // There is an issue with the expiration month.
                    Toast.makeText(this, expirationMonthError.getMessage(), Toast.LENGTH_LONG ).show();
                   // setErrorMessage(expirationMonthError.getMessage());
                    return;
                }
            }
        }
    }

    private void setup() {
        bus.register(this);
        getClientToken();
    }

    public void onScanPress(View v) {
        Intent scanIntent = new Intent(this, CardIOActivity.class);

        // customize these values to suit your needs.
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false

        // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MY_SCAN_REQUEST_CODE) {
            String resultDisplayStr;
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

                // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
                resultDisplayStr = "Card Number: " + scanResult.getRedactedCardNumber() + "\n";
                et_creditCard.setText(scanResult.getFormattedCardNumber().replace(" ", ""));
                // Do something with the raw number, e.g.:
                // myService.setCardNumber( scanResult.cardNumber );

                if (scanResult.isExpiryValid()) {
                    String expiryMonth= String.valueOf(scanResult.expiryMonth);
                    if(scanResult.expiryMonth < 10){
                        expiryMonth = "0" + scanResult.expiryMonth;
                    }
                    resultDisplayStr += "Expiration Date: " + scanResult.expiryMonth + "/" + scanResult.expiryYear + "\n";
                    et_expirationDate.setText(expiryMonth + "/" + scanResult.expiryYear);
                }

                if (scanResult.cvv != null) {
                    // Never log or display a CVV
                    resultDisplayStr += "CVV has " + scanResult.cvv.length() + " digits.\n";
                    et_cvv.setText(scanResult.cvv);
                }

                if (scanResult.postalCode != null) {
                    resultDisplayStr += "Postal Code: " + scanResult.postalCode + "\n";
                }
                Toast.makeText(this, resultDisplayStr, Toast.LENGTH_LONG ).show();
            }
            else {

                resultDisplayStr = "Scan was canceled.";
                Toast.makeText(this, resultDisplayStr, Toast.LENGTH_LONG ).show();
            }
            // do something with resultDisplayStr, maybe display it in a textView
            // resultTextView.setText(resultStr);
        }
        // else handle other activity results
    }

    public void enableButtons(boolean enable){
       btn_submit.setEnabled(enable);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    protected void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            containerView2.setVisibility(show ? View.GONE : View.VISIBLE);
            containerView2.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    containerView2.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            containerView2.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

}
