package mx.com.mobilepoint.travishandroid.Rest.Travish;

import android.text.TextUtils;
import android.util.Log;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.TravishCredentials;

/**
 * Created by IvanIsrael on 05/11/2015.
 */
public class CustomInterceptor implements Interceptor {

    private static final String TAG = CustomInterceptor.class.getName();

    private AccessTokenRequestor tokenRequestor;
    private AccessTokenRepository tokenRepository;
    private String refreshToken;
    private String accessToken;


    public CustomInterceptor(AccessTokenRepository tokenRepository, AccessTokenRequestor tokenRequestor, String accessToken, String refreshToken){

        this.tokenRepository = tokenRepository;
        this.tokenRequestor = tokenRequestor;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;

        Log.i(TAG, "Tokens configured: access[" + accessToken + "], refresh[" + refreshToken + "]");

    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Log.i(TAG, "Access token " + accessToken);

        Request request = requestWithAccessHeader(chain.request());

        // try the request
        Response response = chain.proceed(request);

        if (response.code() == 401) {

            if(refreshToken == null)
                return response;

            // get a new token (Synchronous Retrofit call)
            TravishCredentials tokenResponse = tokenRequestor.refreshToken(refreshToken);

            if(tokenResponse == null)
                return response;

            this.accessToken = tokenResponse.getAccess_token();
            this.refreshToken = tokenResponse.getRefresh_token();

            tokenRepository.saveTokensThenConfigureInterceptor(tokenResponse);

            // create a new request and set its new access token
            Request newRequest = requestWithAccessHeader(request);

            // retry the request
            //TODO: Handle failure again, redirecting to login activity
            return chain.proceed(newRequest);

        }

        // otherwise just pass the original response on
        return response;
    }

    public void setTokens(String accessToken, String refreshToken){
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    private Request requestWithAccessHeader(Request request){

        if(TextUtils.isEmpty(accessToken))
            return request;

        final String authorizationValue = String.format("Bearer %s", accessToken);

        //upsert auth header
        return request.newBuilder().header("Authorization", authorizationValue).build();

    }
}
