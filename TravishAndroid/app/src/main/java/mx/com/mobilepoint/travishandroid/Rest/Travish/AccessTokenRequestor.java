package mx.com.mobilepoint.travishandroid.Rest.Travish;

import java.io.IOException;

import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.TravishCredentials;

/**
 * Created by IvanIsrael on 05/11/2015.
 */
public interface AccessTokenRequestor {
    TravishCredentials refreshToken(String token) throws IOException;
}
