package mx.com.mobilepoint.travishandroid.Rest.Travish;

import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.TravishCredentials;

/**
 * Created by IvanIsrael on 05/11/2015.
 */
public interface AccessTokenRepository {
    boolean saveTokensThenConfigureInterceptor(TravishCredentials accessResponse);
}
