package mx.com.mobilepoint.travishandroid.Events;

import android.util.Log;

import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.User;

/**
 * Created by IvanIsrael on 05/11/2015.
 */
public final class LoginEvent {
    public User user;

    public LoginEvent(User user){
        this.user = user;
    }
}
