package mx.com.mobilepoint.travishandroid.Events;

import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.RegistrationDetails;

/**
 * Created by IvanIsrael on 05/11/2015.
 */
public final class RegisterEvent {
    public RegistrationDetails registerDetails;

    public RegisterEvent(RegistrationDetails registerDetails){
        this.registerDetails = registerDetails;
    }
}
