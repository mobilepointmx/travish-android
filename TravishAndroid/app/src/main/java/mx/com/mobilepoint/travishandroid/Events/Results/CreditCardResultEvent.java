package mx.com.mobilepoint.travishandroid.Events.Results;

import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.ClientBrainTreeResponse;

/**
 * Created by IvanIsrael on 10/11/2015.
 */
public class CreditCardResultEvent {
    public ClientBrainTreeResponse client;
    public CreditCardResultEvent(ClientBrainTreeResponse client) {
        this.client = client;
    }
}
