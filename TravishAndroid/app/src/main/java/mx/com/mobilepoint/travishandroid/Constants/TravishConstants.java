package mx.com.mobilepoint.travishandroid.Constants;

import java.util.HashMap;

/**
 * Created by IvanIsrael on 05/11/2015.
 */
public class TravishConstants {
    public static final String USER_ID = "USER_ID";
    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String REFRESH_TOKEN = "REFRESH_TOKEN";


    public static final String USER_NAME = "USER_NAME";
    public static final String USER_PASSWORD = "USER_PASSWORD";
    public static final String USER = "USER";
    public static final String USER_CREDENTIALS = "USER_CREDENTIALS";

    private static HashMap<String, String> authParameters = new HashMap<String, String>();

    public static final String AUTHORIZE_URL = "https://login.uber.com/oauth/authorize";
    public static final String BASE_URL = "https://login.uber.com/";
    public static final String SCOPES = "profile history_lite history request delivery request_receipt delivery_sandbox";
    public static final String BASE_UBER_URL_V1 = "https://api.uber.com/v1/";
    public static final String BASE_UBER_URL_V1_1 = "https://api.uber.com/v1.1/";
    public static final String CLIEN_ID = "ogEINMRoNBe5O7IFvqSx4YN7Xbg6q9jd";
    public static final String CLIENT_SECRET = "PKY-ZBsxCo_nHQsh6JVxk22DCEEIV4n79XTX2hN-";
    public static final String GRANT_TYPE = "authorization_code";
    public static final String REDIRECT_URL = "https://ec2-52-88-174-216.us-west-2.compute.amazonaws.com:8000/api/schedule/oauth/token/";

}
