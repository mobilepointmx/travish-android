package mx.com.mobilepoint.travishandroid.Events.Results;

import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.UberTokenResponse;


/**
 * Created by IvanIsrael on 12/11/2015.
 */
public final class UbetTokenResultEvent {
    public UberTokenResponse uber;
    public UbetTokenResultEvent(UberTokenResponse body) {
        this.uber = body;
    }
}
