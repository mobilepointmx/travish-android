package mx.com.mobilepoint.travishandroid.Services;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.IOException;

import de.greenrobot.event.EventBus;
import mx.com.mobilepoint.travishandroid.Constants.TravishConstants;
import mx.com.mobilepoint.travishandroid.Events.ApiErrorEvent;
import mx.com.mobilepoint.travishandroid.Events.Results.UberResultEvent;
import mx.com.mobilepoint.travishandroid.Events.UberEvent;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.UserUberCredentials;
import mx.com.mobilepoint.travishandroid.Rest.Travish.TravishApi;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Uber.UberApi;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by IvanIsrael on 11/11/2015.
 */
public class UberService {
    private UberApi sUberAuthService;
    private EventBus bus;
    private static final String TAG = UberService.class.getName();
    public UberService(EventBus bus){
        if (sUberAuthService == null) {
            Retrofit retrofit =  new Retrofit.Builder()
                    .baseUrl(TravishConstants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            sUberAuthService = retrofit.create(UberApi.class);
        }
        this.bus = bus;

    }

    public void onEventMainThread(UberEvent event){
        Log.e(TAG, "Login event");

        Call< UserUberCredentials > resp = sUberAuthService.getUberAuthToken(TravishConstants.CLIENT_SECRET, TravishConstants.CLIEN_ID,  TravishConstants.GRANT_TYPE, event.code ,TravishConstants.REDIRECT_URL);
        resp.enqueue(new Callback<UserUberCredentials>() {
            @Override
            public void onResponse(Response<UserUberCredentials> response, Retrofit retrofit) {
                Log.e("status code: ", String.valueOf(response.code()));
                if(response.isSuccess()){
                    Gson gson = new Gson();
                    String json = gson.toJson(response.body());
                    Log.e("json tags ", json);
                    bus.post(new UberResultEvent(response.body()));
                }else{
                    String error = "";
                    try {
                        if(response.code()==403){
                            JsonElement ele = new JsonParser().parse(response.errorBody().string());
                            bus.post(new ApiErrorEvent(ele.getAsJsonObject().get("error").getAsString()));
                        }else{
                            Log.e("mensaje body: ", response.message().toString());
                            bus.post(new ApiErrorEvent("Error"));
                        }

                    } catch (IOException e) {
                        bus.post(new ApiErrorEvent(e.getMessage()));
                        Log.e("catch mensaje: ", e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("Fallo rest ", t.getMessage());
                bus.post(new ApiErrorEvent(t.getMessage()));

            }
        });

    }


}
