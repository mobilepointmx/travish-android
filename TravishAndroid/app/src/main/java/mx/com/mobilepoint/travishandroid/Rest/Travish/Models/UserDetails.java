package mx.com.mobilepoint.travishandroid.Rest.Travish.Models;

import mx.com.mobilepoint.travishandroid.Activities.Credit_Card;

/**
 * Created by IvanIsrael on 09/11/2015.
 */
public class UserDetails {
    private int id;
    private String username;
    private String createdAt;
    private String updatedAt;
    private String phone;
    private CreditCard creditCard;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }
}