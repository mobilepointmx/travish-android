package mx.com.mobilepoint.travishandroid.Rest.Travish.Models;

import mx.com.mobilepoint.travishandroid.Activities.Credit_Card;

/**
 * Created by IvanIsrael on 09/11/2015.
 */
public class CreditCard {
    private String cardholderName;
    private String brainTreeCustumerId;
    private String createdAt;
    private String updatedAt;
    private BillingAddress billingAddress;


    public String getCardholderName() {
        return cardholderName;
    }

    public void setCardholderName(String cardholderName) {
        this.cardholderName = cardholderName;
    }

    public String getBrainTreeCustimerId() {
        return brainTreeCustumerId;
    }

    public void setBrainTreeCustimerId(String brainTreeCustimerId) {
        this.brainTreeCustumerId = brainTreeCustimerId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public BillingAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(BillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }
}