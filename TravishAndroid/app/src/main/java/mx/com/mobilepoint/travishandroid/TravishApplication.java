package mx.com.mobilepoint.travishandroid;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.OkHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

import de.greenrobot.event.EventBus;
import mx.com.mobilepoint.travishandroid.Activities.Credit_Card;
import mx.com.mobilepoint.travishandroid.Activities.Login;
import mx.com.mobilepoint.travishandroid.Activities.MainActivity;
import mx.com.mobilepoint.travishandroid.Constants.TravishConstants;
import mx.com.mobilepoint.travishandroid.Events.ApiErrorEvent;
import mx.com.mobilepoint.travishandroid.Events.InitialCurrentUserEvent;
import mx.com.mobilepoint.travishandroid.Events.Results.InitialCurrentUserResultEvent;
import mx.com.mobilepoint.travishandroid.Events.Results.LoginResultEvent;
import mx.com.mobilepoint.travishandroid.Rest.Travish.AccessTokenRepository;
import mx.com.mobilepoint.travishandroid.Rest.Travish.CustomInterceptor;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.TravishCredentials;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.UserCredentials;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.UserDetails;
import mx.com.mobilepoint.travishandroid.Rest.Travish.TokenRequestorService;
import mx.com.mobilepoint.travishandroid.Rest.Travish.TravishApi;
import mx.com.mobilepoint.travishandroid.Rest.Travish.TravishRestService;
import mx.com.mobilepoint.travishandroid.Rest.Travish.TravishTokenApi;
import mx.com.mobilepoint.travishandroid.Utils.ApplicationStateHelper;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by IvanIsrael on 05/11/2015.
 */
public class TravishApplication extends Application implements AccessTokenRepository {
    private static final String TAG = TravishApplication.class.getName();
    private EventBus bus = EventBus.getDefault();
    private CustomInterceptor interceptor;
    private SharedPreferences preferences;
    private TravishRestService travishRestService;

    /**
     * when the application starts, the services are initialized.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String accessToken = preferences.getString(TravishConstants.ACCESS_TOKEN, null);
        String refreshToken = preferences.getString(TravishConstants.REFRESH_TOKEN, null);

        interceptor = new CustomInterceptor(this, buildTokenApi(), accessToken, refreshToken);
        try {
            travishRestService = new TravishRestService(buildTravishApi(interceptor), bus);
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        bus.register(this);
        bus.register(travishRestService);

    }

    private TokenRequestorService buildTokenApi() {

        return TokenRequestorService.instance(new Retrofit.Builder()
                .baseUrl(TravishApi.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(TravishTokenApi.class));

    }
    private TravishApi buildTravishApi(CustomInterceptor interceptor) throws CertificateException, IOException {

        OkHttpClient okHttpClient = new OkHttpClient();
        // loading CAs from an InputStream
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
// From https://www.washington.edu/itconnect/security/ca/load-der.crt
        InputStream caInput = this.getResources().openRawResource(R.raw.server);
        HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
        Certificate ca;
        try {
            ca = cf.generateCertificate(caInput);
            Log.e(TAG, "ca=" + ((X509Certificate) ca).getSubjectDN());
        } finally {
            caInput.close();
        }

// Create a KeyStore containing our trusted CAs
        String keyStoreType = KeyStore.getDefaultType();
        KeyStore keyStore = null;
        try {
            keyStore = KeyStore.getInstance(keyStoreType);
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
        try {
            keyStore.load(null, null);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            keyStore.setCertificateEntry("ca", ca);
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }

// Create a TrustManager that trusts the CAs in our KeyStore
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = null;
        try {
            tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            tmf.init(keyStore);
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }

// Create an SSLContext that uses our TrustManager
        SSLContext sslContext = null;
        try {
            sslContext = SSLContext.getInstance("TLS");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            sslContext.init(null, tmf.getTrustManagers(), null);
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }


        okHttpClient.setSslSocketFactory(sslContext.getSocketFactory());
        okHttpClient.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

        okHttpClient.interceptors().add(interceptor);

        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(TravishApi.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(TravishApi.class);
    }

    public void clearSession() {
        preferences.edit().clear().commit();
    }

    @Override
    public boolean saveTokensThenConfigureInterceptor(TravishCredentials accessResponse) {
        Log.e("TAGGERIN SAVETOKEN: ", accessResponse.getAccess_token());
        String accessToken = accessResponse.getAccess_token();
        String refreshToken = accessResponse.getRefresh_token();

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(TravishConstants.ACCESS_TOKEN, accessToken);
        editor.putString(TravishConstants.REFRESH_TOKEN, refreshToken);
        editor.commit();

        interceptor.setTokens(accessToken, refreshToken);

        return true;
    }

    public void onEventMainThread(LoginResultEvent event){

        TravishCredentials response = event.credentials;
        saveTokensThenConfigureInterceptor(response);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(TravishConstants.USER_CREDENTIALS, new Gson().toJson(event.credentials));
        editor.commit();

        TravishApi refreshedApi = null;
        try {
            refreshedApi = buildTravishApi(interceptor);
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        travishRestService.setApi(refreshedApi);
        bus.post(new InitialCurrentUserEvent());

    }

    public void onEventMainThread(ApiErrorEvent event){
        // progress.dismiss();
        Log.e("SOme error", event.message);
    }

    public void onEventMainThread(InitialCurrentUserResultEvent event){

        UserCredentials credentials = getCredentials();
        Log.i(TAG, "onInitialCurrentResultUser");
        if (credentials == null) { //Somethings is wrong, so better restart application
            clearSession();
            ApplicationStateHelper.doRestart(this);
            return;
        }
        Intent intent = (event.user.getCreditCard()== null) ? new Intent(this, Credit_Card.class) : new Intent(this, MainActivity.class);
        //Intent intent = new Intent(this, ProfileWizardActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);


    }

    public UserCredentials getCredentials() {

        String credentialsJson = preferences.getString(TravishConstants.USER_CREDENTIALS, null);

        if (credentialsJson == null)
            return null;

        return new Gson().fromJson(credentialsJson, new TypeToken<UserCredentials>() {
        }.getType());

    }


    public UserDetails getUserDetails() {

        String credentialsJson = preferences.getString(TravishConstants.USER, null);

        if (credentialsJson == null)
            return null;

        return new Gson().fromJson(credentialsJson, new TypeToken<UserDetails>() {
        }.getType());

    }
}
