package mx.com.mobilepoint.travishandroid.Activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.greenrobot.event.EventBus;
import mx.com.mobilepoint.travishandroid.Events.ApiErrorEvent;
import mx.com.mobilepoint.travishandroid.Events.LoginEvent;
import mx.com.mobilepoint.travishandroid.Events.RegisterEvent;
import mx.com.mobilepoint.travishandroid.Events.Results.RegisterResultEvent;
import mx.com.mobilepoint.travishandroid.R;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.RegistrationDetails;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.User;
import mx.com.mobilepoint.travishandroid.TravishApplication;
import mx.com.mobilepoint.travishandroid.Utils.ApplicationStateHelper;

/**
 * Created by luisalejandro on 02/11/2015.
 */
public class CreateUser extends AppCompatActivity {
    private EventBus bus = EventBus.getDefault();
    private CircularProgressView progressView;
    ProgressDialog progress;
    EditText username;
    EditText password;
    EditText repeatPass;
    Button btnSubmit;
    User user;
    private View containerView;
    RegistrationDetails registrationDetails;
    private static final String TAG = CreateUser.class.getName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_createuser_);

        //Register bus for eventbus methods
        bus.register(this);
        containerView = findViewById(R.id.container);
        progressView = (CircularProgressView)findViewById(R.id.pb_loading);
        username = (EditText)findViewById(R.id.username);
        password = (EditText)findViewById(R.id.password);
        repeatPass = (EditText)findViewById(R.id.repeatPassword);
        btnSubmit = (Button)findViewById(R.id.btn_Submit);
        registrationDetails = new RegistrationDetails();


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getInputs();

                if(isEmpty() && isEmail() && validLongitude() && samePassword()){
                    sendInformation();
                    //RegistroTarjeta();
                }else{

                    showProgress(false);
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        Log.e("BACKP", "OnBACKPre");
        finish();
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        bus.unregister(this);
    }
    public boolean samePassword(){
        if(registrationDetails.getPassword().equals(repeatPass.getText().toString().trim()))
            return true;
        Toast.makeText(this, "Repeat password must be same to password", Toast.LENGTH_LONG).show();
        return false;

    }

    public boolean isEmpty(){
        if(registrationDetails.getUsername().isEmpty()){
            Toast.makeText(this, "Username can't be empty", Toast.LENGTH_LONG).show();
            return false;
        }

        if(registrationDetails.getPassword().isEmpty()){
            Toast.makeText(this, "Password can't be empty", Toast.LENGTH_LONG).show();
            return false;
        }
        if(repeatPass.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Repeat password can't be empty", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public void getInputs(){
        registrationDetails.setUsername(username.getText().toString().trim());
        registrationDetails.setPassword(password.getText().toString().trim());
    }

    public boolean isEmail(){
        String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(PATTERN_EMAIL);

        // Match the given input against this pattern
        Matcher matcher = pattern.matcher(registrationDetails.getUsername());
        if(matcher.matches()) return true;

        Toast.makeText(this, "Username must be an email.", Toast.LENGTH_LONG).show();
        return false;
    }

    public void sendInformation(){
        showProgress(true);
        progressView.startAnimation();
       // progress = ProgressDialog.show(CreateUser.this, "Travish", "...", true);
        Log.e(TAG, "En send");
        bus.post(new RegisterEvent(registrationDetails));
    }

    public boolean validLongitude(){
        if(registrationDetails.getPassword().length() > 7) return true;

        Toast.makeText(this, "Password must be greater or equal to 8 characteres.", Toast.LENGTH_LONG).show();
        return false;
    }

    public void onEventMainThread(RegisterResultEvent event){
        //progress.dismiss();
        Log.e(TAG, event.user.getUsername());
        RegistroTarjeta();

    }

    public void onEventMainThread(ApiErrorEvent event){
        showProgress(false);
        progress.dismiss();
        Toast.makeText(this, event.message, Toast.LENGTH_LONG).show();

    }

    public void RegistroTarjeta(){
        user = new User(registrationDetails.getUsername(), registrationDetails.getPassword());

        bus.post(new LoginEvent(user));
//        Intent intent = new Intent(this, Credit_Card.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);
//        finish();
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    protected void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            containerView.setVisibility(show ? View.GONE : View.VISIBLE);
            containerView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    containerView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            containerView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
