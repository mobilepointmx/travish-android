package mx.com.mobilepoint.travishandroid.Rest.Travish.Models;

/**
 * Created by IvanIsrael on 12/11/2015.
 */
public class UberTokenResponse {
    private int id;
    private int UserId;
    private String updatedAt;
    private String createdAt;

    String access_token;

    public String getAccessToken() {
        return access_token;
    }

    String token_type;

    public String getTokenType() {
        return token_type;
    }

    String expires_in;

    public String getExpiresIn() {
        return expires_in;
    }

    String refresh_token;

    public String getRefreshToken() {
        return refresh_token;
    }

    String scope;
    public String getScope() {
        return scope;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
