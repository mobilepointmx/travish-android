package mx.com.mobilepoint.travishandroid.Rest.Travish.Uber;

import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.ClientBrainTreeResponse;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.CreditCardRequest;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.UserUberCredentials;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by IvanIsrael on 11/11/2015.
 */
public interface UberApi {
    @FormUrlEncoded
    @POST("/oauth/token")
    Call<UserUberCredentials> getUberAuthToken(@Field("client_secret") String clientSecret,
                                               @Field("client_id") String clientId,
                                               @Field("grant_type") String grantType,
                                               @Field("code") String code,
                                               @Field("redirect_uri") String redirectUrl);
}
