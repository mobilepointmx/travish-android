package mx.com.mobilepoint.travishandroid.Events.Results;

import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.User;

/**
 * Created by IvanIsrael on 05/11/2015.
 */
public final class RegisterResultEvent {
    public User user;
    public String succes;
    public String message;

    public RegisterResultEvent(User user, String message){
        this.user = user;
        this.message = message;
    }
}
