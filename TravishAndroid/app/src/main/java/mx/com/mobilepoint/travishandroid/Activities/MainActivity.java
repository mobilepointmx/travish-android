package mx.com.mobilepoint.travishandroid.Activities;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import mx.com.mobilepoint.travishandroid.R;
import mx.com.mobilepoint.travishandroid.TravishApplication;
import mx.com.mobilepoint.travishandroid.Utils.ApplicationStateHelper;
import mx.com.mobilepoint.travishandroid.fragments.PanelFragment;


public class MainActivity extends AppCompatActivity {
    DrawerLayout drawerLayout;
    NavigationView navView;
    Toolbar appbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.principal_activity);
        appbar = (Toolbar)findViewById(R.id.appbar);
        setSupportActionBar(appbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu_icon);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.content_frame, new PanelFragment())
                    .commit();
        }
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        navView = (NavigationView)findViewById(R.id.navview);
        //navView.setItemBackground(ContextCompat.getDrawable(R.drawable.ripple));
        //navView.setCheckedItem(0);
        //navView.getMenu().getItem(0).setChecked(true);

        navView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        boolean fragmentTransaction = false;
                        Fragment fragment = null;

                        switch (menuItem.getItemId()) {
                            case R.id.menu_seccion_1:
                                fragment = new PanelFragment();
                                fragmentTransaction = true;
                                menuItem.setChecked(true);

                                break;
                            case R.id.menu_seccion_2:
                                fragment = new PanelFragment();
                                fragmentTransaction = true;
                                menuItem.setChecked(true);

                                break;
                            case R.id.menu_seccion_4:
                                ((TravishApplication) getApplicationContext()).clearSession();
                                ApplicationStateHelper.doRestart(getApplicationContext());
                                break;
                        }

                        if (fragmentTransaction) {
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, fragment)
                                    .commit();

                            menuItem.setChecked(true);
                            getSupportActionBar().setTitle("");
                        }

                        drawerLayout.closeDrawers();

                        return true;
                    }
                });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
            //...
        }

        return super.onOptionsItemSelected(item);
    }

}
