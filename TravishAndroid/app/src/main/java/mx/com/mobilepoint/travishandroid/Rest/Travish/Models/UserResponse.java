package mx.com.mobilepoint.travishandroid.Rest.Travish.Models;

/**
 * Created by IvanIsrael on 05/11/2015.
 */
public class UserResponse {
    private String success;
    private User user;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
