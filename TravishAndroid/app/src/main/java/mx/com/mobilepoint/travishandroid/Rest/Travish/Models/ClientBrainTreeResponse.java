package mx.com.mobilepoint.travishandroid.Rest.Travish.Models;

/**
 * Created by IvanIsrael on 10/11/2015.
 */
public class ClientBrainTreeResponse {

    private String cardholderName;
    private String paymentMethodNonce;
    private int UserId;
    private String customerId;
    private String merchantId;
    private String tokenPaymentMethod;
    private String creditCardToken;
    private String uniqueNumberIdentifier;


    public String getCardholderName() {
        return cardholderName;
    }

    public void setCardholderName(String cardholderName) {
        this.cardholderName = cardholderName;
    }

    public String getPaymentMethodNonce() {
        return paymentMethodNonce;
    }

    public void setPaymentMethodNonce(String paymentMethodNonce) {
        this.paymentMethodNonce = paymentMethodNonce;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getTokenPaymentMethod() {
        return tokenPaymentMethod;
    }

    public void setTokenPaymentMethod(String tokenPaymentMethod) {
        this.tokenPaymentMethod = tokenPaymentMethod;
    }

    public String getCreditCardToken() {
        return creditCardToken;
    }

    public void setCreditCardToken(String creditCardToken) {
        this.creditCardToken = creditCardToken;
    }

    public String getUniqueNumberIdentifier() {
        return uniqueNumberIdentifier;
    }

    public void setUniqueNumberIdentifier(String uniqueNumberIdentifier) {
        this.uniqueNumberIdentifier = uniqueNumberIdentifier;
    }
}
