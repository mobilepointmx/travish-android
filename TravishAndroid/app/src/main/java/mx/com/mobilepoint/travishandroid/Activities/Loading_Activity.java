package mx.com.mobilepoint.travishandroid.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import de.greenrobot.event.EventBus;
import mx.com.mobilepoint.travishandroid.Events.ApiErrorEvent;
import mx.com.mobilepoint.travishandroid.Events.InitialCurrentUserEvent;
import mx.com.mobilepoint.travishandroid.R;
import mx.com.mobilepoint.travishandroid.TravishApplication;

public class Loading_Activity extends AppCompatActivity {
    public static final int milisegundos = 2000;
    private EventBus bus = EventBus.getDefault();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading_);
        bus.register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        bus.post(new InitialCurrentUserEvent());
    }

    public void onEventMainThread(ApiErrorEvent event){
        TravishApplication application = (TravishApplication) getApplication();
        application.clearSession();

        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
        finish();
    }
}

