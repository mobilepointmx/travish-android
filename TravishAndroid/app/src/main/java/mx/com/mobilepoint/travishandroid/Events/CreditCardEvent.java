package mx.com.mobilepoint.travishandroid.Events;

import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.CreditCardRequest;

/**
 * Created by IvanIsrael on 10/11/2015.
 */
public final class CreditCardEvent {
    public CreditCardRequest creditCardRequest;

    public CreditCardEvent(CreditCardRequest creditCardRequest){
        this.creditCardRequest = creditCardRequest;
    }
}
