package mx.com.mobilepoint.travishandroid.Events;

import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.UserUberCredentials;

/**
 * Created by IvanIsrael on 12/11/2015.
 */
public final class UberTokenEvent {
    public UserUberCredentials uber;

    public UberTokenEvent(UserUberCredentials uber){
        this.uber = uber;
    }

}
