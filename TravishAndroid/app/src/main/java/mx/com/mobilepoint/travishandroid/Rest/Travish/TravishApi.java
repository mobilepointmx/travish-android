package mx.com.mobilepoint.travishandroid.Rest.Travish;

import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.BrainTreeToken;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.ClientBrainTreeResponse;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.CreditCardRequest;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.RegistrationDetails;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.TravishCredentials;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.UberTokenRequest;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.UberTokenResponse;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.User;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.UserCredentials;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.UserDetails;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.UserResponse;
import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.UserUberCredentials;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by IvanIsrael on 05/11/2015.
 */
public interface TravishApi {
    static final String API_URL = "https://52.88.174.216:8000";

    @FormUrlEncoded
    @POST("/api/oauth/token/")
    Call<TravishCredentials> loginUser(@Field("username") String username, @Field("password") String password, @Field("client_id") String client_id,
                                     @Field("client_secret") String clien_secret, @Field("grant_type") String grant_type);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("/api/users/")
    Call<UserResponse> signUpUser(@Body RegistrationDetails options);

    @Headers({
            "Content-Type: application/json"
    })
    @GET("/api/payments/token/")
    Call<BrainTreeToken> getBrainTreeToken();

    @Headers({
            "Content-Type: application/json"
    })
    @POST("/api/payments/")
    Call<ClientBrainTreeResponse> postCreditCard(@Body CreditCardRequest options);

    @Headers({
            "Content-Type: application/json"
    })
    @GET("/api/users/")
    Call<UserDetails> getUser();

    @Headers({
            "Content-Type: application/json"
    })
    @POST("/api/schedule/oauth/token/")
    Call<UberTokenResponse> postUberToken(@Body UserUberCredentials options);

}
