package mx.com.mobilepoint.travishandroid.Rest.Travish;

import mx.com.mobilepoint.travishandroid.Rest.Travish.Models.TravishCredentials;
import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by IvanIsrael on 05/11/2015.
 */
public interface TravishTokenApi {
    @FormUrlEncoded
    @POST("/api/oauth/token/")
    Call<TravishCredentials> refreshToken(@Field("refresh_token") String token, @Field("client_id") String clientId,
                                      @Field("client_secret") String clientSecret, @Field("grant_type") String granType);
}
