package mx.com.mobilepoint.travishandroid.Activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.github.rahatarmanahmed.cpv.CircularProgressView;

import de.greenrobot.event.EventBus;
import mx.com.mobilepoint.travishandroid.Constants.TravishConstants;
import mx.com.mobilepoint.travishandroid.Events.ApiErrorEvent;
import mx.com.mobilepoint.travishandroid.Events.Results.UberResultEvent;
import mx.com.mobilepoint.travishandroid.Events.Results.UbetTokenResultEvent;
import mx.com.mobilepoint.travishandroid.Events.UberEvent;
import mx.com.mobilepoint.travishandroid.Events.UberTokenEvent;
import mx.com.mobilepoint.travishandroid.R;
import mx.com.mobilepoint.travishandroid.Services.UberService;

public class UberAuthActivity extends AppCompatActivity {

    WebView webView;
    private EventBus bus = EventBus.getDefault();
    UberService uberService;
    private CircularProgressView progressView;
    private View containerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uber_auth);

        WebView webView = (WebView) findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        uberService = new UberService(bus);
        bus.register(this);
        bus.register(uberService);
        progressView = (CircularProgressView) findViewById(R.id.pb_loading);
        containerView = findViewById(R.id.container);
        containerView.requestFocus();
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                UberAuthActivity.this.setProgress(progress * 1000);
            }
        });

        webView.setWebViewClient(new UberWebViewClient());

        webView.loadUrl(buildUrl());


    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        bus.unregister(this);
        bus.unregister(uberService);

    }


    private String buildUrl() {
        Uri.Builder uriBuilder = Uri.parse(TravishConstants.AUTHORIZE_URL).buildUpon();
        uriBuilder.appendQueryParameter("response_type", "code");
        uriBuilder.appendQueryParameter("client_id", TravishConstants.CLIEN_ID);
        uriBuilder.appendQueryParameter("scope", TravishConstants.SCOPES);
        uriBuilder.appendQueryParameter("redirect_uri", TravishConstants.REDIRECT_URL);
        return uriBuilder.build().toString().replace("%20", "+");
    }
    public void onEventMainThread(UberResultEvent event){
        // progress.dismiss();
        Log.e("UBER token", event.uber.getAccessToken());
        bus.post(new UberTokenEvent(event.uber));
    }

    public void onEventMainThread(UbetTokenResultEvent event){
        showProgress(false);
        Log.e("UBER token", event.uber.getAccessToken());
    }
    public void onEventMainThread(ApiErrorEvent event){
        // progress.dismiss();
        Log.e("UBER Error", event.message);
    }


private class UberWebViewClient extends WebViewClient {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return checkRedirect(url);
    }

    @TargetApi(23)
    @Override
    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        Log.e("eror 23 ", "onreceive");
        showProgress(true);
        progressView.startAnimation();
        if (checkRedirect(request.getUrl().toString())) {
            return;
        }
        Log.e("eror 23 ",  error.getDescription().toString());
        //Toast.makeText(UberAuthActivity.this, "Oh no! " + error.getDescription().toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        Log.e("eror deprecated ", "onreceive");
        showProgress(true);
        progressView.startAnimation();
        if (checkRedirect(failingUrl)) {
            return;
        }
       // Toast.makeText(UberAuthActivity.this, "Oh no! " + description, Toast.LENGTH_SHORT).show();
        Log.e("eror deprecated ",description);
    }

    private boolean checkRedirect(String url) {
        if (url.startsWith(TravishConstants.REDIRECT_URL)) {
            Uri uri = Uri.parse(url);
            Log.e("Query code ", uri.getQueryParameter("code"));
            bus.post(new UberEvent(uri.getQueryParameter("code")));
            return true;
        }

        return false;
    }
}


    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    protected void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            containerView.setVisibility(show ? View.GONE : View.VISIBLE);
            containerView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    containerView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.

            containerView.setVisibility(show ? View.GONE : View.VISIBLE);
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

}
