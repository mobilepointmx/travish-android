package mx.com.mobilepoint.travishandroid.Rest.Travish.Models;

/**
 * Created by IvanIsrael on 10/11/2015.
 */
public class ClientBrainTree {
    private String email;
    private String phone;
    private String paymentMethodNonce;
    private CreditCard creditCard;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPaymentMethodNonce() {
        return paymentMethodNonce;
    }

    public void setPaymentMethodNonce(String paymentMethodNonce) {
        this.paymentMethodNonce = paymentMethodNonce;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }
}
