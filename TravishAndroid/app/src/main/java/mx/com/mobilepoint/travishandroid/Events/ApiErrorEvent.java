package mx.com.mobilepoint.travishandroid.Events;

/**
 * Created by IvanIsrael on 05/11/2015.
 */
public final class ApiErrorEvent {
    public String message;

    public ApiErrorEvent(String message){
        this.message = message;
    }
}
